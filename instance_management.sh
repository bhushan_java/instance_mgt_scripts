#!/bin/sh -xv
echo "************************************************"
echo "Start"
echo "************************************************"
cd ~
mkdir downloads
mkdir opt
cd downloads/

echo "--------Downloading--------"
wget http://mirrors.linuxeye.com/jdk/jdk-7u80-linux-x64.tar.gz
wget http://a.mbbsindia.com/tomcat/tomcat-7/v7.0.69/bin/apache-tomcat-7.0.69.tar.gz
wget http://nginx.org/download/nginx-1.8.0.tar.gz
echo "--------Downloads ends--------"

echo "--------Extracting JDK and setting the path parameters--------"
tar -xvf jdk-7u80-linux-x64.tar.gz -C /home/ec2-user/opt
echo "export JAVA_HOME=/home/ec2-user/opt/jdk-7u80-linux-x64" >> ~/.bashrc

echo "--------Extracting NGINX and setting the path parameters--------"
tar -xvf nginx-1.8.0.tar.gz -C /home/ec2-user/opt
echo "export NGINX_HOME=/home/ec2-user/opt/nginx-1.8.0" >> ~/.bashrc


echo "--------Setting PATH variable--------"
echo "export PATH=\$PATH:\$JAVA_HOME/bin:\$NGINX_HOME/bin" >> ~/.bashrc

cd ~
source ~/.bashrc

echo "------------END---------------"
